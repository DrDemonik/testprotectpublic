﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Class1
    {

        
            /// <summary>
            /// все равно, что private int ;
            /// </summary>
            int _int;
            /// <summary>
            /// поле доступно только из текущего класса
            /// </summary>
            private int _privateInt;
            /// <summary>
            /// доступно из текущего класса и производных классов
            /// </summary>
            protected int _protectedInt;
            /// <summary>
            /// доступно в любом месте программы
            /// </summary>
            internal int _internalInt; //
                                       /// <summary>
                                       /// доступно в любом месте программы и из классов-наследников
                                       /// </summary>
            protected internal int _protectedInternalInt; // 
                                                          /// <summary>
                                                          /// доступно в любом месте программы, а также для других программ и сборок
                                                          /// </summary>
            public int _publicInt; // 

            private protected int _privatProtectedInt;
            //protected private int _protectedPrivateInt; // доступно из текущего класса и производных классов, которые определены в том же проекте

            /*private void Display__int()
            {
                Console.WriteLine($"Переменная _int = {_int}");
            }

            public void Display__privateInt()
            {
                Console.WriteLine($"Переменная _privateInt = {_privateInt}");
            }

            internal void Display__protectedInt()
            {
                Console.WriteLine($"Переменная _protectedInt = {_protectedInt}");
            }

            protected void Display__internalInt()
            {
                Console.WriteLine($"Переменная _internalInt = {_internalInt}");
            }*/
        
    }

    public class Class2 : Class1
    {
        public Class2()
        {
            _privatProtectedInt = 0;            
        }
    }
}
