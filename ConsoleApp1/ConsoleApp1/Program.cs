using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mynamespace;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            Modificators m = new Modificators();
            m._publicInt = 0;            
            m._internalInt = 0;
            m._protectedInternalInt = 0;

            Test t = new Test();
            t._publicInt = 0;
            t._internalInt = 0;
            t._protectedInternalInt = 0;

            ClassLibrary1.Class1 class1 = new ClassLibrary1.Class1();
            class1._publicInt = 0;

            Test2 test2 = new Test2();
            test2._publicInt = 0;
            

            ClassLibrary1.Class2 class2 = new ClassLibrary1.Class2();
            class2._publicInt = 0;
           

            Console.WriteLine("Hello World!");
            Console.ReadKey();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 
        }
    }

    class Test :Modificators
    { }

    class Test2 : ClassLibrary1.Class1
    {
        public Test2()
        {
            _publicInt = 0;
            _protectedInt = 0;
            _protectedInternalInt = 0;
            
        }
    }
}
